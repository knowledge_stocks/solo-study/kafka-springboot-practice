package com.kakaruu.practice.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class ConsumerService {

    @KafkaListener(topics = "test1", groupId = "test1")
    public void onMessage(String message) throws IOException {
        log.info("받은 메시지 : " + message);
    }
}
