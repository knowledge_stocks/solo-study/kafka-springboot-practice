package com.kakaruu.practice.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaSpringbootPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaSpringbootPracticeApplication.class, args);
	}

}
