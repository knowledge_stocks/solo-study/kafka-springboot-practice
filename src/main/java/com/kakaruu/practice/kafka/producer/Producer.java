package com.kakaruu.practice.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class Producer {

    @Value("${spring.kafka.producer.bootstrap-servers}")
    private String servers;

    @Value("${spring.kafka.producer.key-serializer}")
    private String keySerializer;

    @Value("${spring.kafka.producer.value-serializer}")
    private String valueSerializer;

    public void publish(String topic, String message) {
        Properties configs = new Properties();
        configs.put("bootstrap.servers", servers);
        configs.put("key.serializer", keySerializer);
        configs.put("value.serializer", valueSerializer);

        KafkaProducer producer = new KafkaProducer<String, String>(configs);
        ProducerRecord record = new ProducerRecord<String, String>(topic, message);

        producer.send(record);

        producer.close();
    }
}
