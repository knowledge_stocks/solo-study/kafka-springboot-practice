package com.kakaruu.practice.kafka.api.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProducerService {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public void publishMessage(String topic, String message) {
        kafkaTemplate.send(topic, message);
    }
}
