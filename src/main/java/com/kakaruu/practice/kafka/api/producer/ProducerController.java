package com.kakaruu.practice.kafka.api.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class ProducerController {

    private final ProducerService producerService;

    @PostMapping(value = "/test1Message")
    public void sendTest1Message(@RequestParam("msg") String message) {
        producerService.publishMessage("test1", message);
    }
}
